package jsonconfig

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
)

// JSONConfig contains configuration loaded from JSON file.
type JSONConfig struct {
	PixelPath     string
	QueueName     string
	RmqURI        string
	ServerAddress string
}

// NewJSONConfig creates new JSONConfig instance from file.
func NewJSONConfig(filename string) (result *JSONConfig, err error) {
	bytes, err := ioutil.ReadFile(filename)
	if err != nil {
		return
	}

	err = json.Unmarshal(bytes, &result)

	return
}

// GetPixelPath gets pixel location path.
func (js JSONConfig) GetPixelPath() string {
	return js.PixelPath
}

// GetQueueName gets queue name.
func (js JSONConfig) GetQueueName() string {
	return js.QueueName
}

// GetRmqURI gets RabbitMQ connection string.
func (js JSONConfig) GetRmqURI() string {
	return js.RmqURI
}

// GetServerAddress gets local HTTP server address.
func (js JSONConfig) GetServerAddress() string {
	return js.ServerAddress
}

// ReadFile read picture.
func ReadFile() *os.File{
	img, err := os.Open("pixel.png")
	if err != nil{
		log.Fatal(err)
	}
	defer img.Close()

	return img
	// wr.Header().Set("Content-Type", "image/png")
	// io.Copy(wr, img)
}