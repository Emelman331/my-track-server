package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"time"

	jsonconfig "track-serv/json-config"
)

// Application entry point
func main() {
	time.Sleep(time.Second*10)

	var filePath string
	flag.StringVar(&filePath ,"path", "", "config path")
	flag.Parse()

	if filePath == ""{
		log.Fatalln("not valid path!")
	}
	
	fmt.Println("start server")
	// Load ConsulConfig
	cfg, err := jsonconfig.NewJSONConfig(filePath)
	if err != nil {
		log.Fatal(err)
	}

	// Prepare pixel
	pixel, err := ioutil.ReadFile(cfg.GetPixelPath())
	if err != nil {
		log.Fatal(err)
	}

	// Prepare publisher
	publisher, err := NewRmqPublisher(cfg.GetRmqURI(), cfg.GetQueueName())
	if err != nil {
		log.Fatal(err)
	}

	// Start tracking server
	log.Fatal(NewTrackServer(cfg, pixel, publisher).Start())
}
