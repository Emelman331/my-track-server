package main

import (
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strconv"
)

// TrackServer represents tracking server
type TrackServer struct {
	config    Config // !Interface
	pixel     []byte
	publisher Publisher // !Interface
}

// NewTrackServer creates new TrackServer instance
func NewTrackServer(config Config, pixel []byte, publisher Publisher) *TrackServer {
	return &TrackServer{
		config:    config,
		pixel:     pixel,
		publisher: publisher,
	}
}

func (ts *TrackServer) rootHandler(writer http.ResponseWriter, request *http.Request) {
	log.Printf("Processing request %v", request.URL)

	query := request.URL.Query()

	aid, err := strconv.Atoi(query.Get("a"))
	if err != nil {
		writer.Write([]byte(err.Error()))
		return // !
	}

	event := Event{
		ActionID:    aid,
		Profile:     query.Get("p"),
		RedirectURL: query.Get("r"),
	}

	err = ts.publisher.Publish(event)
	if err != nil {
		fmt.Println("Publish event")
		writer.Write([]byte(err.Error()))
		return // !
	}

	if event.ActionID == ActionPixel {
		writer.Header().Set("Content-Type", "image/png")
		writer.Write(ts.pixel)
		return // !
	}

	targetURL, err := url.ParseRequestURI(event.RedirectURL)
	if err != nil {
		writer.Write([]byte(err.Error()))
		return // !
	}

	http.Redirect(writer, request, targetURL.String(), 303)
}

// Start runs tracking server
func (ts *TrackServer) Start() error {
	http.HandleFunc("/", ts.rootHandler)
	return http.ListenAndServe(ts.config.GetServerAddress(), nil)
}
