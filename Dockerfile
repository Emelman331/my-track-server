FROM golang:1.13


WORKDIR /track-serv
COPY track-serv ./track-serv
COPY json-config /track-serv/json-config
COPY rabbit-config /track-serv/rabbit-config
ADD pixel.png /track-serv/

CMD chmod +x track-serv && ./track-serv
