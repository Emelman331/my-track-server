package main

//const for actions enum
const (
	// Tracking actions

	_           = iota // 0
	ActionOpen         // 1
	ActionClick        // 2
	ActionPixel        // 3
)
