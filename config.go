package main

// Config contains application configuration
type Config interface {
	GetPixelPath() string
	GetQueueName() string
	GetRmqURI() string
	GetServerAddress() string
}
