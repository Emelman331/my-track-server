package main

import (
	"github.com/hashicorp/consul/api"
)

// ConsulConfig contains configuration loaded from JSON file
type ConsulConfig struct {
	PixelPath     string
	QueueName     string
	RmqURI        string
	ServerAddress string
}

// NewConsulConfig creates new ConsulConfig instance
func NewConsulConfig() (result *ConsulConfig, err error) {
	kv, err := LoadKeyValue()
	if err != nil {
		return nil, err
	}

	err = result.PutValuesToConsul(kv)
	if err != nil {
		return nil, err
	}

	pair, _, err := kv.Get("PixelPath", nil)
	if err != nil {
		return nil, err
	}

	pixel := string(pair.Value)

	pair, _, err = kv.Get("QueueName", nil)
	if err != nil {
		return nil, err
	}

	queue := string(pair.Value)

	pair, _, err = kv.Get("RmqURI", nil)
	if err != nil {
		return nil, err
	}

	uri := string(pair.Value)

	pair, _, err = kv.Get("ServerAddress", nil)
	if err != nil {
		return nil, err
	}

	server := string(pair.Value)

	return &ConsulConfig{
		PixelPath:     pixel,
		QueueName:     queue,
		RmqURI:        uri,
		ServerAddress: server,
	}, nil
}

// LoadKeyValue to load api function.
func LoadKeyValue() (*api.KV, error) {
	cfg := api.DefaultConfig()

	client, err := api.NewClient(cfg)
	if err != nil {
		return nil, err
	}

	return client.KV(), err
}

func (cfg *ConsulConfig) PutValuesToConsul(kv *api.KV) error {
	p := &api.KVPair{Key: "PixelPath", Value: []byte("jsonconfig/config.json")}

	_, err := kv.Put(p, nil)
	if err != nil {
		return err
	}

	m := &api.KVPair{Key: "QueueName", Value: []byte("track-server")}

	_, err = kv.Put(m, nil)
	if err != nil {
		return err
	}

	f := &api.KVPair{Key: "RmqURI", Value: []byte("amqp://guest:guest@rabbit:5672/")}

	_, err = kv.Put(f, nil)
	if err != nil {
		return err
	}

	s := &api.KVPair{Key: "ServerAddress", Value: []byte(":8080")}

	_, err = kv.Put(s, nil)
	if err != nil {
		return err
	}

	return nil
}

// GetPixelPath gets pixel location path.
func (cfg ConsulConfig) GetPixelPath() string {
	return cfg.PixelPath
}

// GetQueueName gets queue name.
func (cfg ConsulConfig) GetQueueName() string {
	return cfg.QueueName
}

// GetRmqURI gets RabbitMQ connection string.
func (cfg ConsulConfig) GetRmqURI() string {
	return cfg.RmqURI
}

// GetServerAddress gets local HTTP server address.
func (cfg ConsulConfig) GetServerAddress() string {
	return cfg.ServerAddress
}
