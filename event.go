package main

// Event represents tracking event
type Event struct {
	ActionID    int    `json:"a" xml:"a"`
	Profile     string `json:"p" xml:"p"`
	RedirectURL string `json:"r" xml:"r"`
}
